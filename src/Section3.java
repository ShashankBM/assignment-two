import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

@RunWith(Parameterized.class)
public class Section3 {
	WebDriver dr;
	
	@Parameterized.Parameters()
	public static Collection<Object[]> data() {
		Object[][] data=new Object[][] {
			{"ie"},{"chrome"},{"edge"},{"firefox"}
		};
		return Arrays.asList(data);
	}
	
	@Parameterized.Parameter(0)
	public String s;

	@Before
	public void Before() {
		switch(s) {
		case "ie":
			System.setProperty("webdriver.ie.driver", "D:\\20111917\\ASSIGNMENT2\\Drivers\\IEDriverServer.exe");
			dr=new InternetExplorerDriver();
			break;
		case "chrome":
			System.setProperty("webdriver.chrome.driver", "D:\\20111917\\ASSIGNMENT2\\Drivers\\chromedriver.exe");
			dr=new ChromeDriver();
			break;
		case "edge":
			System.setProperty("webdriver.edge.driver", "D:\\20111917\\ASSIGNMENT2\\Drivers\\MicrosoftWebDriver.exe");
			dr=new EdgeDriver();
			break;
		case "firefox":
			System.setProperty("webdriver.gecko.driver", "D:\\20111917\\ASSIGNMENT2\\Drivers\\geckodriver.exe");
			dr=new FirefoxDriver();
			break;
		}
		dr.get("http://shop.demoqa.com/");
	}
	
	@Test
	public void Test1() {
		assertNotEquals("LINK NOT FOUND",-1,dr.getCurrentUrl().indexOf("http://shop.demoqa.com/"));
	}
	
	@Test
	public void Test2() {
		assertNotEquals("TITLE NOT FOUND",-1,dr.getTitle().indexOf("Shoptools"));
	}
	@After
	public void After() {
		dr.close();
	}
}
