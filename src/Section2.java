
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Section2 {
	static WebDriver dr;
	
	@Rule public TestName t=new TestName();
	
	@BeforeClass
	public static void BeforeClass() {
		System.setProperty("webdriver.chrome.driver", "D:\\20111743\\Assignment_2\\Drivers\\chromedriver.exe");
		dr=new ChromeDriver();
	}
	
	@Before
	public void Before() {
		dr.get("http://shop.demoqa.com/");
	}
	
	@Test
	public void Test1() {
		assertNotEquals("LINK NOT FOUND",-1,dr.getCurrentUrl().indexOf("http://shop.demoqa.com/"));
	}
	
	@Test
	public void Test2() {
		assertNotEquals("TITLE NOT FOUND",-1,dr.getTitle().indexOf("Shoptools"));
	}
	
	@After
	public void After() {
		System.out.println("test "+t.getMethodName()+" completed");
	}
	
	@AfterClass
	public static void AfterClass() {
		dr.close();
	}
}
	