

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Section_1 {
	WebDriver dr;
	
	@Before
	public void Before() {
		System.setProperty("webdriver.chrome.driver", "D:\\20111917\\Assignment_2\\Drivers\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("http://shop.demoqa.com/");
	}
	
	@Test
	public void Test1() {
		assertNotEquals("Link unavailable",-1,dr.getCurrentUrl().indexOf("http://shop.demoqa.com/"));
	}
	
	@Test
	public void Test2() {
		assertNotEquals("Title not found",-1,dr.getTitle().indexOf("Shoptools"));
	}
	
	@After
	public void After() {
		dr.close();
	}
}