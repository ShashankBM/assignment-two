

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Section4 {
	WebDriver dr;
	
	@org.junit.Before
	public void Before() {
		System.setProperty("webdriver.chrome.driver", "D:\\20111917\\ASSIGNMENT2\\Drivers\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("http://shop.demoqa.com/");
	}
	
	@BeforeEach
	public void BeforeEach() {
		System.setProperty("webdriver.chrome.driver", "D:\\20111917\\ASSIGNMENT2\\Drivers\\chromedriver.exe");
		dr=new ChromeDriver();
		dr.get("http://shop.demoqa.com/");
	}
	
	@RepeatedTest(3)
	@Disabled
	@Tag("LinkTest")
	public void Test1() {
		assertNotEquals("LINK NOT FOUND",-1,dr.getCurrentUrl().indexOf("http://shop.demoqa.com/"));
	}
	
	@Test(timeout=1000, expected=AssertionError.class)
	@Tag("TitleTest")
	public void Test2() {
		assertNotEquals("TITLE NOT FOUND",-1,dr.getTitle().indexOf("Shoptools"));
	}
	
	@AfterEach
	public void AfterEach() {
		dr.close();
	}
	
	@org.junit.After
	public void After() {
		dr.close();
	}
}